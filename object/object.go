package object

import (
	"backend/handler"
	"context"
	"fmt"
	"log"

	"github.com/mongodb/mongo-go-driver/bson"
	"github.com/mongodb/mongo-go-driver/bson/primitive"
)

var OBJECT_COLLECTION = "object"

type Object struct {
	ID   interface{} `json:"id,omitempty" bson:"_id,omitempty"`
	Name string      `json:"name" bson:"name"`
}
type ObjectTableView struct {
	ID     interface{} `json:"id,omitempty" bson:"_id,omitempty"`
	Name   string      `json:"name" bson:"name"`
	Amount float32     `json:"amount" bson:"amount"`
}

func InsertObject(o Object) bool {

	return true
}
func removeObject(id string) bool {

	return true
}
func (o Object) update() bool {
	objectID, _ := primitive.ObjectIDFromHex(o.ID.(string))
	o.ID = objectID
	filter := bson.D{{"_id", o.ID}}
	collection := handler.MONGO.Collection(OBJECT_COLLECTION)
	_, err := collection.ReplaceOne(context.TODO(), filter, o)
	if err != nil {
		log.Fatal(err)
	}
	if err != nil {
		fmt.Println(err)
		return false
	}
	return true
}

func selectAllObject() []ObjectTableView {
	collection := handler.MONGO.Collection(OBJECT_COLLECTION)

	query := `[
		{
		  "$lookup": {
			"from": "object_join_table", 
			"localField": "_id", 
			"foreignField": "objectID", 
			"as": "Object"
		  }
		}
	  ]`

	filter := []bson.M{}
	err := bson.UnmarshalExtJSON([]byte(query), true, &filter)

	var results []ObjectTableView
	cur, err := collection.Aggregate(context.TODO(), filter)
	if err != nil {
		fmt.Println(err)
	}
	for cur.Next(context.TODO()) {

		var elem ObjectTableView
		err := cur.Decode(&elem)
		if err != nil {
			fmt.Println(err)
		}
		results = append(results, elem)
	}
	return results
}
