package object

import (
	"encoding/json"
	"io/ioutil"
	"net/http"

	"github.com/gin-gonic/gin"
)

func GetAllObject(c *gin.Context) {
	c.Header("Content-Type", "application/json")
	objList := selectAllObject()
	c.JSON(http.StatusOK, objList)
}

func UpdateObject(c *gin.Context) {
	r := c.Request
	w := c.Writer
	var obj Object
	body, _ := ioutil.ReadAll(r.Body)
	json.Unmarshal(body, &obj)
	ok := obj.update()
	if ok {
		c.Header("Content-Type", "application/json")
		c.JSON(http.StatusOK, gin.H{
			"status":  "ok",
			"message": "succes",
		})
	} else {
		w.WriteHeader(http.StatusBadRequest)
	}
}
func DeleteObject(c *gin.Context) {
	id := c.Param("id")
	w := c.Writer
	ok := removeObject(id)
	if ok {
		c.Header("Content-Type", "application/json")
		c.JSON(http.StatusOK, gin.H{
			"status":  "ok",
			"message": "succes",
		})
	} else {
		w.WriteHeader(http.StatusBadRequest)
	}
}
func AddObject(c *gin.Context) {

	r := c.Request
	w := c.Writer
	var obj Object
	body, _ := ioutil.ReadAll(r.Body)
	json.Unmarshal(body, &obj)
	ok := InsertObject(obj)
	if ok {
		c.Header("Content-Type", "application/json")
		c.JSON(http.StatusOK, gin.H{
			"status":  "ok",
			"message": "succes",
		})
	} else {
		w.WriteHeader(http.StatusBadRequest)
	}

}
