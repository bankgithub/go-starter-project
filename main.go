package main

import (
	"backend/route"

	"github.com/gin-contrib/static"
)

func main() {
	// Set the router as the default one shipped with Gin
	router := route.NewRouter()
	// Serve frontend static files

	router.Use(static.Serve("/", static.LocalFile("../dist", true)))
	// Setup route group for the API

	router.Run(":3000")
}
