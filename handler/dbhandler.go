package handler

import (
	"context"
	"encoding/json"
	"fmt"

	"log"
	"os"

	"github.com/mongodb/mongo-go-driver/mongo"
)

type AllConfig struct {
	DataDBHost     string `json:"dataDBHost"`
	DataDBPort     string `json:"dataDBPort"`
	DataDBUser     string `json:"dataDBUser"`
	DataDBPassword string `json:"dataDBPassword"`
	DataDBName     string `json:"dataDBName"`
	DataDBMongo    string `json:"dataDBMongo"`
	AuthDBHost     string `json:"authDBHost"`
	AuthDBPort     string `json:"authDBPort"`
	AuthDBUser     string `json:"authDBUser"`
	AuthDBPassword string `json:"authDBPassword"`
	AuthDBName     string `json:"authDBName"`
	Admin          string `json:"admin"`
	RedisAddr      string `json:"redisAddr"`
	RedisPassword  string `json:"redisPassword"`
	RedisDB        int    `json:"redisDB"`
}

var MONGO *mongo.Database

func init() {

	fmt.Println("init db")

	//MONGO = connectMongo()

}

func LoadConfiguration(file string) AllConfig {
	var config AllConfig
	configFile, err := os.Open(file)
	defer configFile.Close()
	if err != nil {
		log.Println(err.Error())
	}
	jsonParser := json.NewDecoder(configFile)
	jsonParser.Decode(&config)
	return config
}

func connectMongo() *mongo.Database {
	config := LoadConfiguration("./dbconfig.json")
	client, err := mongo.NewClient(config.DataDBMongo)
	if err != nil {
		panic(err)
	}
	err = client.Connect(context.TODO())
	if err != nil {
		log.Fatal(err)
	} else {
		log.Println("Successfully connected to mongo")
	}
	return client.Database(config.DataDBName)
}
