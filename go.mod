module backend

require (
	github.com/gin-contrib/cors v0.0.0-20190101123304-5e7acb10687f
	github.com/gin-contrib/static v0.0.0-20181225054800-cf5e10bbd933
	github.com/gin-gonic/gin v1.3.0
	github.com/go-stack/stack v1.8.0 // indirect
	github.com/golang/snappy v0.0.0-20190218232222-2a8bb927dd31 // indirect
	github.com/google/go-cmp v0.2.0 // indirect
	github.com/mongodb/mongo-go-driver v0.2.0
	github.com/tidwall/pretty v0.0.0-20180105212114-65a9db5fad51 // indirect
	github.com/xdg/scram v0.0.0-20180814205039-7eeb5667e42c // indirect
	github.com/xdg/stringprep v1.0.0 // indirect
	golang.org/x/crypto v0.0.0-20190211182817-74369b46fc67 // indirect
	golang.org/x/text v0.3.0 // indirect
)
