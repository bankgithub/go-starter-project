package route

import (
	"backend/object"

	"github.com/gin-gonic/gin"
)

func AddobjectRoute(routerGroup *gin.RouterGroup) {

	routerGroup.GET("/objects", authMiddleware("object", "view"), object.GetAllObject)
	routerGroup.POST("/object", authMiddleware("object", "add"), object.AddObject)
	routerGroup.PUT("/object", authMiddleware("object", "edit"), object.UpdateObject)
	routerGroup.DELETE("/object/:id", authMiddleware("object", "edit"), object.DeleteObject)
}
